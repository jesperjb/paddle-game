using UnityEngine;
using System.Collections;

public class Barrier : MonoBehaviour {

	void OnCollisionEnter( Collision col ) {
		foreach (ContactPoint contact in col.contacts) {
			if( contact.thisCollider == collider ) {
				if(col.gameObject.tag == "Ball") {
					GameController.EndGame();
				}
			}
		}
	}
}
