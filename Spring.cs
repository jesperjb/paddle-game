using UnityEngine;
using System.Collections;

public class Spring : MonoBehaviour {
	
	public float rotationSpeed = 10.0f;
	public float rotationMax = 0.3f;
	public float sensitivity = 0.5f;
	public float addedForce = 300.0f;
	
	// Game score
	public float scoreValue = 1.0f;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetAxis("Vertical") != 0) {
			float rotation = Input.GetAxis("Vertical") * rotationSpeed * Time.deltaTime;
			if(transform.rotation.z > rotationMax && Input.GetAxis("Vertical") > 0) {
				rotation = 0;
			}
			if(transform.rotation.z < -rotationMax && Input.GetAxis("Vertical") < 0) {
				rotation = 0;
			}
			transform.Rotate(new Vector3(0, 0, rotation));
		}
	}
	
	void OnCollisionEnter( Collision col ) {
		foreach (ContactPoint contact in col.contacts) {
			if( contact.thisCollider == collider ) {
				// This is the paddle's contact point
				float multiplier = (contact.point.x - transform.position.x) * sensitivity;
				
				contact.otherCollider.rigidbody.AddForce(addedForce * multiplier, 0, 0);
				
				GameController.AddScore(scoreValue);
				gameObject.audio.Play();
			}
		}
	}
}
