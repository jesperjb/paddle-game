using UnityEngine;
using System.Collections;

public class Ball : MonoBehaviour {

	// Use this for initialization
	void Start () {
		Vector3 randomPosition = new Vector3(Random.Range(-150.0F, 150.0F), transform.position.y, transform.position.z);
		transform.position = randomPosition;
		rigidbody.AddForce(new Vector3(0, -600.0f, 0));
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
