using UnityEngine;
using System.Collections;

public class Slider : MonoBehaviour {
	
	public float speed = 10.0f;
	public float border = 100f;
	
	// Update is called once per frame
	void Update () {
		if(Input.GetAxis("Horizontal") != 0) {
			float movement = Input.GetAxis("Horizontal") * speed * Time.deltaTime;
			transform.Translate(movement, 0, 0,Space.World);
			
			if (transform.position.x > border) {
				transform.position = new Vector3( border, transform.position.y, transform.position.z );
			}
			if (transform.position.x < -border) {
				transform.position = new Vector3( -border, transform.position.y, transform.position.z );
			}
		
		}
	}
}
